=== Grace: Instagram Feed Gallery ===
Contributors: looks_awesome, awesomeoman
Donate link: http://looks-awesome.com/
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Tags: instagram, instagram feed, instagram gallery, instagram posts, instagram grid, instagram wall, instagram hub, social, social board, social feed, social grid, social hub, social wall, social stream, stream, masonry grid, social wall, social media wall, social feed, social media feed, social media aggregator
Requires at least: 3.6
Tested up to: 4.9
Stable tag: 1.0.1.40
== Description ==

The most advanced free plugin for creating graceful Instagram feed media walls of Instagram public posts. This feature-rich plugin lets you aggregate and showcase posts of Instagram accounts, hashtags and locations. And the great thing is that you can mix any of Instagram feeds in the same social media wall or carousel. Add eye-catching Instagram gallery to your site in fast and easy way!

https://www.youtube.com/watch?v=4N76wUXL-AU

= What You Get Absolutely For Free =

* 5 Instagram sources — Any public account, hashtags, like feeds, location ID and location coordinates.
* Amazing gallery layouts — Including masonry and carousel image slider.
* Seamless API connection — No complicated setups. Install plugin, click one button for API connection and you are ready to go. Yes, you will get access to Instagram public content!
* DIY card template builder — Use drag-n-drop to have card content the way you want it to show.
* Content filtering — Add admin filters to exclude any posts by word, by username, by URL. Clean your galleries from trolls and spam!
* Social sharing buttons — Let your visitors share posts without leaving your website. Improve your website metrics and grow social capital.
* Highly customizable — Change colors, elements composition and ordering, design gallery layout and so much more!
* Responsive design — 100% responsive with swipes support for sliding on mobile devices.
* Smart resource loading — Script and styles are loaded only when shortcode is detected on page.
* Translation ready — Use tools like Loco Translate to translate plugin. Then you can send translation files to us!
* Enjoyable interaction — Beautiful animations and hover effects.
* GDPR compliant - no visitor data stored anywhere

Looking for even more? More fantastic features are available in PRO version! [Check out PRO demo](https://social-streams.com/grace/demo/ "Grace PRO version demo")

= What Features You Can Unlock in PRO VERSION =

* Two additional layouts — Grid of equal height cards and Justified Gallery to showcase your content in even more creative ways.
* Lightbox galleries — Fantastic look with attractive animations, smart media preloading, Instagram comments, post meta, sharing tools etc.
* Posts approval system — this is the best possible feature to protect your brand or service from unwanted posts. Pre-moderate your galleries in manual mode!
* Beautiful profile headings — Run personal account gallery with awesome profile header with user avatar, account info and followers/following data.
* Multi photos in one post support — Lightbox supports multi photo posts and creates slider just like in native Instagram app.
* Filters and search — Let your visitors filter content by username, hashtag and even use live search feature.
* Include only rule — Additionaly to EXCLUDE ALL you can use INCLUDE ONLY rule to pull posts with specific word, hashtag, username etc

Upgrade to PRO! [Check out PRO version page](http://social-streams.com/grace "Grace PRO version")

= Performance and Security =

Smart caching with server task allows to deliver content blazingly fast without long page loading. It's especially crucial when many feeds are pulled or you have big traffic. It's great to use this plugin for streaming your and any public Instagram content!

Plugin doesn't expose your private details (like tokens, app IDs and app secrets) to browser so we provide 100% security for any sensitive data.

= How it works =

You create streams in Grace Instagram Feed admin and copy generated shortcodes. Then you insert these shortcodes in any block on any page of your site. When Instagram feed is shown first time, it's got cached (takes more time for initial caching but then it will show blazingly fast) and rest of visitors see this cache for cache lifetime. So for example if you have set 20 min cache and 10000 users visit your site within these 20 min, it's **only one time** plugin will request API to pull data, for the rest of time users will see cached data almost instantly. Considering that server runs cache renew task in background it guarantees plugin's amazing performance.

== Installation ==

**This section describes how to install the plugin and get it working**


= Automatic installation =

To do an automatic install of Grace, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "Grace Instagram Feed" and click Search Plugins. Once you have found it you can install it by simply clicking "Install Now".


= Manual installation =

**Uploading via WordPress Dashboard**

1. Download `insta-flow.zip`
2. Navigate to the 'Add New' in the plugins dashboard
3. Navigate to the 'Upload' area
4. Select `insta-flow.zip` from your computer
5. Click 'Install Now'
6. Activate the plugin in the Plugin dashboard

**Using FTP**

1. Download `insta-flow.zip`
2. Extract the `insta-flow` directory to your computer
3. Upload the `insta-flow` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard

The WordPress codex contains [instructions on how to install a WordPress plugin](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).



= Updating =

You can use automatic update to update the plugin safely.


== Frequently Asked Questions ==

= Where I can find documentation? =
Please find Grace knowledge base [here](http://docs.social-streams.com/).

= Where I can find settings? =
Settings are located in WP Admin on first-level, look for Social Apps / Grace Instagram menu item.

= Do you provide support? =
Grace Lite edition is distributed "as is" and doesn't contain support services. However it can be our good will to help you. Please, be advised that fast and premium support is provided with Pro version only.

== Screenshots ==

1. Grace Instagram Feed on page

2. Plugin Admin page

3. Styling settings page

== Changelog ==

= Version 1.0.1.40 =
* Fix: Admin error message wording

= Version 1.0.1.39 =
* Change: Code optimizations, readme update

= Version 1.0.1.38 =
* Change: Typo in code

= Version 1.0.1.37 =
* Change: Requirement raised from PHP 5.3 to PHP 5.4

= Version 1.0.1.36 =
* Fix: For 'Media with given code does not exist' error

= Version 1.0.1.35 =
* Update: New Instagram API changes

= Version 1.0.1.34 =
* Update: Further Instagram API integration

= Version 1.0.1.33 =
* Update: For 4th April Instagram API changes

= Version 1.0.1.32 =
* Change: settings defaults

= Version 1.0.1.31 =
* Change: general settings defaults

= Version 1.0.1.30 =
* Change: Cron custom interval compat

= Version 1.0.1.29 =
* Change: admin settings defaults

= Version 1.0.1.28 =
* Fix: admin PHP notices

= Version 1.0.1.27 =
* Fix: for previous change

= Version 1.0.1.26 =
* Fix: video URLs cache

= Version 1.0.1.25 =
* Fix: reverting previous change that broke plugin

= Version 1.0.1.24 =
* Fix: smart sorting

= Version 1.0.1.23 =
* Fix: feed errors could break admin page in some cases

= Version 1.0.1.22 =
* Fix: admin CSS errors

= Version 1.0.1.21 =
* Tweak: admin css

= Version 1.0.1.20 =
* Tweak: admin css

= Version 1.0.1.19 =
* Tweak: remove debug

= Version 1.0.1.18 =
* Tweak: admin UI

= Version 1.0.1.17 =
* Tweak: admin controls tooltips

= Version 1.0.1.16 =
* Tweak: animation loading

= Version 1.0.1.15 =
* Tweak: admin code

= Version 1.0.1.14 =
* Fix: admin notification cookie name conflict

= Version 1.0.1.13 =
* Tweak: admin hints

= Version 1.0.1.12 =
* Fix: admin CSS

= Version 1.0.1.11 =
* Fix: JS issue in Internet Explorer for filters

= Version 1.0.1.10 =
* Tweak: deactivation details submission

= Version 1.0.1.9 =
* Change: symbol used for filtering by URL part

= Version 1.0.1.8 =
* New scheme for plugin versioning to prevent updates conflict with pro version

= Version 1.0.8 =
* Tweak: Cleaning up admin code

= Version 1.0.7 =
* Tweak: Admin code performance improvements

= Version 1.0.6 =
* Added: Support team member is added as contributor in readme.txt

= Version 1.0.5 =
* Fix for support ticket emails

= Version 1.0.4 =
* Fix for admin link and text typos

= Version 1.0.3 =
* Deactivation actions added

= Version 1.0.2 =
* Initial release