<?php

/*
 Template Name: About Us
 */


$page_link = get_permalink($post->ID);


 ?>
<?php get_header(); ?>



<title><?php echo $page_title; ?></title>

<link href="css/about_us.css?ver1.2" rel="stylesheet" media="screen">

<body>

<?php include "nav_bar.php" ?>

<div class="main_div container">
	<div class="col-sm-6 main_img_div">
		<img src="img/cosmo_logo_6.png">
	</div>
	<div class="col-sm-6">
		<p>
			The company was setup in 1996 as an import & distribution 
company for personal care products, over the years it has 
grown to be one of the leading companies in the field.
</p>
<p>
We have decided to change the whole business model in 
2016 and to shift from importation to local manufacturing. 
</p>
<p>
It has been our policy and intention from day one to 
offer products that match, if not exceed the quality of the 
products that we used to import in terms of quality, design, 
fragrance and to make sure that the products will meet the 
various needs of the Egyptian consumers.
</p>
<p>
Our mission is to provide Egyptian made product with a 
much better quality and value proposition than the imported 
products that are offered to Egyptian consumers primarily 
in the field of personal care products.
		</p>
	</div>
</div>
<?php get_footer(); ?>


<script>

$( document ).ready(function() {

$('.serv_item').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('#modal-5 .md-content').html($(this).parent().find('.hidden_image_div').html());

    $('.md-close').trigger('click');
    $('.md_trigger_service').trigger('click');

  });


});

window.onload = function() {


$('.overlay').fadeOut(500);


}

$(window).resize(function () {


});

$(window).scroll(function (event) {
    
});

</script>

