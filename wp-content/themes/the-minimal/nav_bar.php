<div class="overlay" id="loading">
</div>

<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
	?>
	<div class="desk_nav">
	<div class="nav_bar container">
		<div class="col-sm-2 navs nav_3">
			<div class="nav_3_inner">
				<div class="ls_div">
					<a class="ls_idle_link" href="">
						<span class="ls_idle">ENG</span>
					</a>
					|
					<span class="ls_active">ARA</span>
				</div>
				<div class="link_div">
					<a href="https://facebook.com/BlendsArabia/" target="_blank">
						<img src="img/fb_icon_new.png">
					</a>
					<a href="https://www.instagram.com/blends.arabia/" target="_blank">
						<img src="img/insta_icon_new.png">
					</a>
				</div>
			</div>
		</div>
		<div class="col-sm-2">
		</div>
		<a href="<?= get_site_url(); ?>/home-ar">
			<div class="col-sm-4 navs nav_2">
				<img src="img/logo.png">
			</div>
		</a>
		<div class="col-sm-4 navs nav_1">
			<span>a product by</span>
			<img src="img/cosm_logo.png">
		</div>
	</div>
	<div class="nav_bar_2 collapse navbar-collapse">
	    <ul>
			<li>
				<a href="<?= get_permalink(292); ?>">
					<span class="center exp_tab">إكتشف حلولنا</span>
				</a>
			</li>
			<li>
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="center exp_tab">منتجات بليندز</span>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		          <a class="dropdown-item" href="<?= get_permalink(314); ?>">
		          	<span class="">شامبو</span>
		          </a>
		          <a class="dropdown-item" href="<?= get_permalink(309); ?>">
		          	<span class="">بلسم</span>
		          </a>
		          <a class="dropdown-item" href="<?= get_permalink(318); ?>">
		          	<span class="">ماسك الشعر</span>
		          </a>
		          <a class="dropdown-item" href="<?= get_permalink(316); ?>">
		          	<span class="">سيرم</span>
		          </a>
		        </div>
			</li>
			<li>
				<a href="<?= get_permalink(280); ?>" class="no_link">
					<span class="center exp_tab">عن الشركة</span>
				</a>
			</li>
			<li>
				<a class="menu_span" href="<?= get_site_url(); ?>/home-ar#letyoubeyou" slideID="letyoubeyou">
					<span class="center exp_tab let_span">#letyoubeyou</span>
				</a>
			</li>
		</ul>
	</div>
</div>


<div class="fake_head">
</div>
<nav class="navbar navbar-default navbar-fixed-top mobile_nav">
    <div class="container-fluid">
      <div class="navbar-header">
      	<div class="ls_div ls_mobile">
			<a class="ls_idle_link" href="">
				<span class="ls_idle">ENG</span>
			</a>
			|
			<span class="ls_active">ARA</span>
		</div>
        <a class="navbar-brand mobile_links" href="<?= get_site_url(); ?>/home-ar" >
          <img src="img/logo.png">
        </a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div id="navbar" class="navbar-collapse collapse nav_bar_2">
        <ul class="nav navbar-nav">
          <li>
          	<a href="<?= get_permalink(292); ?>">
          		<span class="">إكتشف حلولنا</span>
          	</a>
          </li>
          <li>
          	<a href="#" class="pro_btn">
          		<span class="">منتجات بليندز</span>
          	</a>
          	<ul class="drop_mobile">
				<li>
					<a class="dropdown-item" href="<?= get_permalink(314); ?>">
		          		<span class="">شامبو</span>
		          	</a>
				</li>
				<li>
					<a class="dropdown-item" href="<?= get_permalink(309); ?>">
	          		<span class="">بلسم</span>
	          </a>
				</li>
				<li>
					<a class="dropdown-item" href="<?= get_permalink(318); ?>">
	          		<span class="">ماسك الشعر</span>
	          </a>
				</li>
				<li>
					<a class="dropdown-item" href="<?= get_permalink(316); ?>">
	          		<span class="">سيرم</span>
	          </a>
				</li>
			</ul>
          </li>
          <li>
          	<a href="<?= get_permalink(280); ?>" class="no_link">
          		<span class="">عن الشركة</span>
          	</a>
          </li>
          <li>
          	<a class="menu_span" href="<?= get_site_url(); ?>/home-ar#letyoubeyou" slideID="letyoubeyou">
          		<span class="let_span">#letyoubeyou</span>
          	</a>
          </li>
        </ul>
      </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
  </nav>
	<?php
}
else
{
	?>
		<div class="desk_nav">
		<div class="nav_bar container">
			<div class="col-sm-4 navs nav_1">
				<span>a product by</span>
				<img src="img/cosm_logo.png">
			</div>
			<a href="<?= get_site_url(); ?>">
				<div class="col-sm-4 navs nav_2">
					<img src="img/logo.png">
				</div>
			</a>
			<div class="col-sm-2">
			</div>
			<div class="col-sm-2 navs nav_3">
				<div class="nav_3_inner">
					<div class="ls_div">
						<span class="ls_active">ENG</span>
						|
						<a class="ls_idle_link" href="">
							<span class="ls_idle">ARA</span>
						</a>
					</div>
					<div class="link_div">
						<a href="https://facebook.com/BlendsArabia/" target="_blank">
							<img src="img/fb_icon_new.png">
						</a>
						<a href="https://www.instagram.com/blends.arabia/" target="_blank">
							<img src="img/insta_icon_new.png">
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="nav_bar_2 collapse navbar-collapse">
		    <ul>
				<li>
					<a href="<?= get_permalink(19); ?>">
						<span class="center exp_tab">Explore Solutions</span>
					</a>
				</li>
				<li>
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="center exp_tab">Products</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			          <a class="dropdown-item" href="<?= get_permalink(65); ?>">
			          	<span class="">Shampoo</span>
			          </a>
			          <a class="dropdown-item" href="<?= get_permalink(68); ?>">
			          	<span class="">Conditioner</span>
			          </a>
			          <a class="dropdown-item" href="<?= get_permalink(70); ?>">
			          	<span class="">Hair Mask</span>
			          </a>
			          <a class="dropdown-item" href="<?= get_permalink(72); ?>">
			          	<span class="">Hair Serum</span>
			          </a>
			        </div>
				</li>
				<li>
					<a href="<?= get_permalink(280); ?>">
						<span class="center exp_tab">About Us</span>
					</a>
				</li>
				<li>
					<a class="menu_span" href="<?= get_site_url(); ?>#letyoubeyou" slideID="letyoubeyou">
						<span class="center exp_tab">#letyoubeyou</span>
					</a>
				</li>
			</ul>
		</div>
	</div>


	<div class="fake_head">
	</div>
	<nav class="navbar navbar-default navbar-fixed-top mobile_nav">
	    <div class="container-fluid">
	      <div class="navbar-header">
	      	<div class="ls_div ls_mobile">
				<span class="ls_active">ENG</span>
				|
				<a class="ls_idle_link" href="">
					<span class="ls_idle">ARA</span>
				</a>
			</div>
	        <a class="navbar-brand mobile_links" href="<?= get_site_url(); ?>" >
	          <img src="img/logo.png">
	        </a>
	        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	          <span class="sr-only">Toggle navigation</span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	        </button>
	      </div>
	      <div id="navbar" class="navbar-collapse collapse nav_bar_2">
	        <ul class="nav navbar-nav">
	          <li>
	          	<a href="<?= get_permalink(19); ?>">
	          		<span class="">Explore Solutions</span>
	          	</a>
	          </li>
	          <li>
	          	<a href="#" class="pro_btn">
	          		<span class="">Products</span>
	          	</a>
	          	<ul class="drop_mobile">
					<li>
						<a class="dropdown-item" href="<?= get_permalink(65); ?>">
			          		<span class="">Shampoo</span>
			          	</a>
					</li>
					<li>
						<a class="dropdown-item" href="<?= get_permalink(68); ?>">
		          		<span class="">Conditioner</span>
		          </a>
					</li>
					<li>
						<a class="dropdown-item" href="<?= get_permalink(70); ?>">
		          		<span class="">Hair Mask</span>
		          </a>
					</li>
					<li>
						<a class="dropdown-item" href="<?= get_permalink(72); ?>">
		          		<span class="">Hair Serum</span>
		          </a>
					</li>
				</ul>
	          </li>
	          <li>
	          	<a href="<?= get_permalink(280); ?>">
	          		<span class="">About Us</span>
	          	</a>
	          </li>
	          <li>
	          	<a class="menu_span" href="<?= get_site_url(); ?>#letyoubeyou" slideID="letyoubeyou">
	          		<span class="">#letyoubeyou</span>
	          	</a>
	          </li>
	        </ul>
	      </div><!--/.nav-collapse -->
	    </div><!--/.container-fluid -->
	  </nav>
	<?php
}
?>