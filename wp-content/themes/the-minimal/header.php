<!doctype html>
<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
	?>
	<html dir="rtl">
	<?php
}
else
{
	?>
	<html dir="ltr">
	<?php
}
?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/the-minimal"/>


<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<?php wp_head() ?>
</head>

<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
	?>
	<link href="css/style_ar.css?ver=2.12" rel="stylesheet" media="screen">
	<!-- <link href="css/style_ar.css" rel="stylesheet" media="screen"> -->
	<?php
}
else
{
	?>
	<link href="css/style.css?ver=1.157" rel="stylesheet" media="screen">
	<?php
}
?>

<style type="text/css">
html {
    margin-top: 0 !important;
}
</style>

<?php 
	echo do_shortcode('[do_widget id=polylang-3]');
?>

