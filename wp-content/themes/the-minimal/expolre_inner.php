<?php

/*
 Template Name: Explore Inner
 */


$product_cat = get_field('cateogry', get_the_ID());
$page_banner = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
$color = get_field('color', 'product_category_'.$product_cat->term_id);
// print_r($product_cat);die;
$products_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'products', 'suppress_filters' => 0,
	'order' => 'ASC',
	'tax_query' => array(
            array(
                'taxonomy' => 'product_category',
                'field' => 'term_id',
                'terms' => $product_cat->term_id,
            )
        ));

$products = get_posts( $args );
// print_r($products);die;
foreach ( $products as $post ) :   setup_postdata( $post );

$title = $post -> post_title;
$link = $post -> guid;
$link = str_replace('https', "http", $link);
$link = str_replace('blendsbrand.com', "8-bitlabs.com/blends", $link);
$image = wp_get_attachment_url( get_post_thumbnail_id($post -> ID) );
// $related = get_field('related_products', get_the_ID());

$products_array[] = array('title' => $title, 'link' => $link, 'image' => $image);

endforeach;
wp_reset_postdata();

// print_r($products_array);die;

 ?>
<?php get_header(); ?>



<title><?php echo $page_title; ?></title>

<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
	?>
	<link href="css/explore_inner_ar.css?ver1.4" rel="stylesheet" media="screen">
	<?php
}
else
{
	?>
	<link href="css/explore_inner.css?ver1.4" rel="stylesheet" media="screen">
	<?php
}
?>

<body>

<?php include "nav_bar.php" ?>

<div class="banner_div">
	<img src="<?= $page_banner; ?>">
</div>
<div class="main_div container">
	<?php
	foreach ($products_array as $key => $value) {
		?>
		<a href="<?= $value['link']?>">
			<div class="cat_div col-sm-4">
				<img src="<?= $value['image']?>">
				<span><?= $value['title']?></span>
			</div>
		</a>
		<?php
	}
	?>
</div>

<input type='hidden' class="color" value="<?= $color; ?>"/>
<?php get_footer(); ?>


<!-- <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script> -->

<script>

$( document ).ready(function() {

$('.serv_item').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('#modal-5 .md-content').html($(this).parent().find('.hidden_image_div').html());

    $('.md-close').trigger('click');
    $('.md_trigger_service').trigger('click');

  });


});

window.onload = function() {


$('.overlay').fadeOut(500);

var maxHeight = 0;

$(".cat_div").each(function(){
   if ($(this).outerHeight() > maxHeight) { maxHeight = $(this).outerHeight(); }
});

$(".cat_div").height(maxHeight);

}


$(window).scroll(function (event) {
    
});

</script>

