<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
	?>
		<div class="footer container">
			<div class="col-sm-4 foot_3 foot_ar_1">
				<div class="foot_3_inner">
					<span>Join the Conversation</span>
					<img class="foot_3_img_1" src="img/foot_logo_2.jpg">
					<img class="foot_3_img_2" src="img/foot_cosm.png">
				</div>
			</div>
			<div class="col-sm-4 foot_2">
			</div>
			<div class="col-sm-4 foot_1">
				<img src="img/footer_log.png">
				<span>Stay Updated</span>
				<input type="text" class="form-control form_inputs" placeholder="البريد الالكتروني">
				<ul>
					<li>Faq</li>
					<li>About Us</li>
					<li>Help</li>
					<li>Sitemap</li>
					<li>Cookies</li>
					<li>Terms & Conditions</li>
				</ul>
			</div>
			<div class="col-sm-4 foot_3 foot_ar_2">
				<div class="foot_3_inner">
					<span>Join the Conversation</span>
					<img class="foot_3_img_1" src="img/foot_logo_2.jpg">
					<img class="foot_3_img_2" src="img/foot_cosm.png">
				</div>
			</div>
			<div class="col-sm-4 sm_foot">
				<a href="https://www.instagram.com/blends.arabia/" target="_blank">
					<img src="img/insta_icon_new.png">
				</a>
				<a href="https://facebook.com/BlendsArabia/" target="_blank">
					<img src="img/fb_icon_new.png">
				</a>
			</div>
		</div>
	<?php
}
else
{
	?>
		<div class="footer container">
			<div class="col-sm-4 foot_1">
				<img src="img/footer_log.png">
				<span>Stay Updated</span>
				<input type="text" class="form-control form_inputs" placeholder="Email">
				<ul>
					<li>Faq</li>
					<li>About Us</li>
					<li>Help</li>
					<li>Sitemap</li>
					<li>Cookies</li>
					<li>Terms & Conditions</li>
				</ul>
			</div>
			<div class="col-sm-4 foot_2">
			</div>
			<div class="col-sm-4 foot_3">
				<div class="foot_3_inner">
					<span>Join the Conversation</span>
					<img class="foot_3_img_1" src="img/foot_logo_2.jpg">
					<img class="foot_3_img_2" src="img/foot_cosm.png">
				</div>
			</div>
			<div class="col-sm-4 sm_foot">
				<a href="https://facebook.com/BlendsArabia/" target="_blank">
					<img src="img/fb_icon_new.png">
				</a>
				<a href="https://www.instagram.com/blends.arabia/" target="_blank">
					<img src="img/insta_icon_new.png">
				</a>
			</div>
		</div>
	<?php
}
?>

<?php 

//wp_footer(); 

?>

</body>
</html>

<script>

$( document ).ready(function() {

var lang = $('.lang-item a').attr('href');
$('.ls_idle_link').attr('href', lang);

var height = $(".foot_1").outerHeight();
$(".foot_3").height(height);

var windowWidth = $(window).width();
var windowHeight = $(window).height();

if(windowWidth > 767)
{
  // var newHeight = parseInt($('.navbar').css('height'));
  // $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  // newHeight = '20px';
  $('.fake_head').css('height', newHeight);
}


$('.nav_bar_2 ul li').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeOut(500);
});


$('.no_link').click(function(e) {

e.preventDefault();

});
$('.pro_btn').click(function(e) {

	e.preventDefault();
  $('.drop_mobile').slideToggle();

});


$('.nav_3').css('height', $('.nav_bar').css('height'));

});

$(window).resize(function () {

var height = $(".foot_1").outerHeight();
$(".foot_3").height(height);

var windowWidth = $(window).width();
var windowHeight = $(window).height();

if(windowWidth > 767)
{
  // var newHeight = parseInt($('.navbar').css('height'));
  // $('.fake_head').css('height', newHeight);
}

else
{
  var newHeight = parseInt($('.mobile_nav').css('height'));
  // newHeight = '20px';
  $('.fake_head').css('height', newHeight);
}

$('.nav_3').css('height', $('.nav_bar').css('height'));

});


</script>
