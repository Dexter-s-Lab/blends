<?php

/*
 Template Name: Landing Page
 */

$page_banner = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
// print_r($page_banner);die;


$products_res = array();
$products_array = array();
$args = array( 'numberposts' => 1, 'post_type'=>'products', 'suppress_filters' => 0, 'orderby' => 'rand',
	'order'    => 'ASC',
	'tax_query' => array(
    array(
      'taxonomy' => 'product_type',
      'field' => 'slug',
      'terms' => 'conditioner'
    )
  )
);
$products = get_posts( $args );
$products_res[] = $products[0];


$args = array( 'numberposts' => 1, 'post_type'=>'products', 'suppress_filters' => 0, 'orderby' => 'rand',
	'order'    => 'ASC',
	'tax_query' => array(
    array(
      'taxonomy' => 'product_type',
      'field' => 'slug',
      'terms' => 'hair-mask'
    )
  )
);
$products = get_posts( $args );
$products_res[] = $products[0];


$args = array( 'numberposts' => 1, 'post_type'=>'products', 'suppress_filters' => 0, 'orderby' => 'rand',
	'order'    => 'ASC',
	'tax_query' => array(
    array(
      'taxonomy' => 'product_type',
      'field' => 'slug',
      'terms' => 'hair-serum'
    )
  )
);
$products = get_posts( $args );
$products_res[] = $products[0];


$args = array( 'numberposts' => 1, 'post_type'=>'products', 'suppress_filters' => 0, 'orderby' => 'rand',
	'order'    => 'ASC',
	'tax_query' => array(
    array(
      'taxonomy' => 'product_type',
      'field' => 'slug',
      'terms' => 'shampoo'
    )
  )
);
$products = get_posts( $args );
$products_res[] = $products[0];


// print_r($products_res);die;
foreach ( $products_res as $post ) :   setup_postdata( $post );

$title = $post -> post_title;
$link = $post -> guid;
$link = str_replace('https', "http", $link);
$link = str_replace('blendsbrand.com', "8-bitlabs.com/blends", $link);
$image = wp_get_attachment_url( get_post_thumbnail_id($post -> ID) );

$tax_cat = get_the_terms( $post -> ID, 'product_category' );
$tax_cat = $tax_cat[0];
$color = get_field('color', $tax_cat->taxonomy.'_'.$tax_cat->term_id);
// print_r($color);die;

$products_array[] = array('title' => $title, 'link' => $link, 'image' => $image
	, 'color' => $color);

endforeach;
wp_reset_postdata();

// print_r($products_array);die;

 ?>
<?php get_header(); ?>


<title><?php echo $page_title; ?></title>


<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
  ?>
  <link href="css/home_style_ar.css?ver1.8" rel="stylesheet" media="screen">
  <?php
}
else
{
  ?>
  <link href="css/home_style.css?ver1.11" rel="stylesheet" media="screen">
  <?php
}
?>


<body>


<?php include "nav_bar.php" ?>

<div class="banner_div">
  <?php
  echo do_shortcode('[rev_slider alias="home"]');
  ?>
</div>

<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
  ?>
      <div class="explore_div">
        <span class="exp_span_1">إكتشف حلولنا</span>
        <div class="main_div container">
        <?php
          foreach ($products_array as $key => $value) {
            ?>
            <a href="<?= $value['link']?>">
              <div class="cat_div col-sm-3" colorID="<?= $value['color']?>">
                <img src="<?= $value['image']?>">
                <span><?= $value['title']?></span>
              </div>
            </a>
            <?php
          }
          ?>
        </div>
        <span class="exp_span_1 exp_span_2">Stay Connected</span>
      </div>
  <?php
}
else
{
  ?>
    <div class="explore_div">
      <span class="exp_span_1">Explore Our Products</span>
      <div class="main_div container">
      <?php
        foreach ($products_array as $key => $value) {
          ?>
          <a href="<?= $value['link']?>">
            <div class="cat_div col-sm-3" colorID="<?= $value['color']?>">
              <img src="<?= $value['image']?>">
              <span><?= $value['title']?></span>
            </div>
          </a>
          <?php
        }
        ?>
      </div>
      <span class="exp_span_1">Stay Connected</span>
    </div>
  <?php
}
?>

<div class="insta_div" id="letyoubeyou">
	<!-- <span class="insta_span_1">Top Posts</span> -->
	<div class="insta_div_inner">
		<!-- <span class="insta_span_2">Top Posts</span> -->
		<?php 
    echo do_shortcode('[grace id="1"]');
    //echo do_shortcode('[wdi_feed id="1"]');
    //echo wdi_feed(array('id'=>'1'));
    ?>

	</div>
</div>

<?php get_footer(); ?>


<!-- <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script> -->

<script>

$( document ).ready(function() {

$('.menu_span').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    var slideID = $(this).attr('slideID');
    var post = $('#'+slideID);

    
    var new_height = parseInt($('.navbar').css('height'));
    // var new_height = 0;

    var windowWidth = $(window).width();
    if(windowWidth > 767)
    new_height = 0;


    var position = post.position().top - $(window).scrollTop();
    $('html, body').stop().animate({
        'scrollTop': post.offset().top - new_height
    }, 900, 'swing', function () {
        
    });

    var flag1 = $('.mobile_nav').is(':visible');
      var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

      if(flag1 && flag2) 
      $('.navbar-toggle').click();

  });

});

window.onload = function() {


// $('.carousel').carousel({
//   interval: 2000
// });

// $('.fake_head').height('80px');

$('.overlay').fadeOut(500);



}

$(window).resize(function () {

    // $('.owl-demo_2 .item p').css('width', $('.demo_img').css('width'));
    // $('.fake_head').height('80px');
});

$(window).scroll(function (event) {
    
});

</script>

