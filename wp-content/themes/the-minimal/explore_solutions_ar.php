<?php

/*
 Template Name: Explore Solutions AR
 */


$page_link = get_permalink($post->ID);
// print_r($page_link);die;
$categs = get_terms([
    'taxonomy' => 'product_category',
    'hide_empty' => false,
    'order' => 'ASC'
]);

$cat_array = array();
foreach ($categs as $key => $value) {

	// print_r($value);die;
	
	$thumb = get_field('thumb', $value->taxonomy.'_'.$value->term_id);
	$color = get_field('color', $value->taxonomy.'_'.$value->term_id);
	$cat_array[] = array('name' => $value->name, 'thumb' => $thumb, 'desc' => $value->description
		,'color' => $color, 'link' => $page_link.$value->slug);

	// print_r($cat_array);die;

}

 ?>
<?php get_header(); ?>



<title><?php echo $page_title; ?></title>


<link href="css/explore_solutions_ar.css?ver1.1" rel="stylesheet" media="screen">

<body>

<?php include "nav_bar.php" ?>

<div class="main_div container">
	<?php
	foreach ($cat_array as $key => $value) {
		?>
		<a href="<?= $value['link']?>">
			<div class="cat_div col-sm-4" colorID="<?= $value['color']?>">
				<img src="<?= $value['thumb']?>">
				<span><?= $value['desc']?></span>
			</div>
		</a>
		<?php
	}
	?>
</div>
<?php get_footer(); ?>


<!-- <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script> -->

<script>

$( document ).ready(function() {

$('.serv_item').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('#modal-5 .md-content').html($(this).parent().find('.hidden_image_div').html());

    $('.md-close').trigger('click');
    $('.md_trigger_service').trigger('click');

  });


});

window.onload = function() {


$('.overlay').fadeOut(500);

// var maxHeight = 0;

// $(".cat_div").each(function(){
//    if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
// });

//$(".cat_div").height(maxHeight);


// $(".cat_div").css('height', maxHeight+ 'px');



var width = $('.cat_div img').css('width');
//$('.cat_div span').css('width', width);


}

$(window).resize(function () {


});

$(window).scroll(function (event) {
    
});

</script>

