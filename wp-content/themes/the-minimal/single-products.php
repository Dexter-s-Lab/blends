<?php


$product_id = get_the_ID();

$tax_cat = get_the_terms( get_the_ID(), 'product_category' );
$tax_cat = $tax_cat[0];


$tax_type = get_the_terms( get_the_ID(), 'product_type' );
$tax_type = $tax_type[0];

// print_r($tax_cat);
// print_r($tax_type);

global $post;
$post_slug=$post->post_name;

$post_slug = explode("-ar", $post_slug);
$post_slug = $post_slug[0];
$page_link = get_site_url().'/products/'.$post_slug;

// print_r($page_link);

$final_slug  = $tax_type -> slug;
$final_slug = explode("-ar", $final_slug);
$final_slug = $final_slug[0];

$page_banner = get_field($final_slug, $tax_cat->taxonomy.'_'.$tax_cat->term_id);
$type_title = get_field($final_slug.'_title', $tax_cat->taxonomy.'_'.$tax_cat->term_id);
$type_desc = get_field($final_slug.'_desc', $tax_cat->taxonomy.'_'.$tax_cat->term_id);
// $type_desc = $tax_cat -> description;
$type_how_to_use = get_field($final_slug.'_how_to_use', $tax_cat->taxonomy.'_'.$tax_cat->term_id);
$type_key_benefits = get_field($final_slug.'_key_benefits', $tax_cat->taxonomy.'_'.$tax_cat->term_id);
$type_key_ingredients = get_field($final_slug.'_key_ingredients', $tax_cat->taxonomy.'_'.$tax_cat->term_id);
$type_range = get_field($final_slug.'_range', $tax_cat->taxonomy.'_'.$tax_cat->term_id);

$main_image = get_field('main_image', get_the_ID());
$related_products_array = get_field('related_products', get_the_ID());
// print_r($related_products_array);die;

// print_r($tax_cat);
// print_r($tax_type);die;

foreach ($related_products_array as $key => $value) {
	
	$tax_type = get_the_terms( $value -> ID, 'product_type' );
	$tax_type = $tax_type[0];
	$link = $value -> guid;
	$link = str_replace('https', "http", $link);
	$link = str_replace('blendsbrand.com', "8-bitlabs.com/blends", $link);
	// print_r($tax_type);;die;
	$related_products[] = array("name" => $tax_type -> name
		, "thumb" => get_field('related_thumb', $value -> ID)
		,"link" => $link);
}



 ?>
<?php get_header(); ?>



<title><?php echo $page_title; ?></title>

<?php

$lang = get_bloginfo("language");

if($lang == 'ar')
{
	?>
	<link href="css/single_product_ar.css?ver1.5" rel="stylesheet" media="screen">
	<?php
}
else
{
	?>
	<link href="css/single_product.css?ver1.10" rel="stylesheet" media="screen">
	<?php
}
?>

<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">

<body>

<?php include "nav_bar.php" ?>

<div class="banner_div">
	<img src="<?= $page_banner; ?>">
</div>

<?php

if($lang == 'ar')
{
	?>
	<input type= "hidden" value="<?= $page_link; ?>" class="last_link">
		<div class="main_div container">
			<div class="main_right main_right_mobile col-sm-6">
				<img src="<?= $main_image; ?>">
			</div>
			<div class="main_right main_right_desk col-sm-6">
				<img src="<?= $main_image; ?>">
			</div>
			<div class="main_left col-sm-6">
				<span class="main_titles"><?= $type_title; ?></span>
				<div class="top_rating">
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
				</div>
				<p class="desc_p"><?= $type_desc; ?></p>
				<div class="mid_div">
					<span class="main_titles">الفوائد و المميزات</span>
					<p class="main_p"><?= $type_key_benefits; ?></p>
				</div>
				<div class="mid_div">
					<span class="main_titles key_title">المواد الاساسية</span>
					<p class="main_p"><?= $type_key_ingredients; ?></p>
				</div>
				<span class="main_titles"><?= $type_range; ?></span>
			</div>
		</div>
	<?php
}
else
{
	?>
	<input type= "hidden" value="<?= $page_link; ?>-ar" class="last_link">
		<div class="main_div container">
			<div class="main_right main_right_mobile col-sm-6">
				<img src="<?= $main_image; ?>">
			</div>
			<div class="main_left col-sm-6">
				<span class="main_titles"><?= $type_title; ?></span>
				<div class="top_rating">
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
				</div>
				<p class="desc_p"><?= $type_desc; ?></p>
				<span class="main_titles">How To Use</span>
				<p class="main_p"><?= $type_how_to_use; ?></p>
				<div class="mid_div">
					<span class="main_titles">Key Benefits</span>
					<p class="main_p"><?= $type_key_benefits; ?></p>
				</div>
				<div class="mid_div">
					<span class="main_titles key_title">Key Ingredients</span>
					<p class="main_p"><?= $type_key_ingredients; ?></p>
				</div>
				<span class="main_titles"><?= $type_range; ?></span>
			</div>
			<div class="main_right main_right_desk col-sm-6">
				<img src="<?= $main_image; ?>">
			</div>
		</div>
	<?php
}
?>
<div class="rel_div container">
<?php

foreach ($related_products as $key => $value) {
	
	?>
	<a href="<?= $value['link']; ?>">
		<div class="related_div">
			<img src="<?= $value['thumb']; ?>">
			<span><?= $value['name']; ?></span>
		</div>
	</a>
	<?php
}
?>
</div>
<?php get_footer(); ?>


<!-- <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script> -->

<script>

$( document ).ready(function() {



});

window.onload = function() {


$('.overlay').fadeOut(500);

var maxHeight = 0;

$(".cat_div").each(function(){
   if ($(this).outerHeight() > maxHeight) { maxHeight = $(this).outerHeight(); }
});

$(".cat_div").height(maxHeight);

var lang = $('.last_link').val();
$('.ls_idle_link').attr('href', lang);

}

$(window).resize(function () {

// var maxHeight = 0;

// $(".cat_div").each(function(){
//    if ($(this).outerHeight() > maxHeight) { maxHeight = $(this).outerHeight(); }
// });

// $(".cat_div").height(maxHeight);
// console.log(maxHeight);

});

$(window).scroll(function (event) {
    
});

</script>

